package com.prex.base.server.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prex.base.api.dto.DictDTO;
import com.prex.base.api.entity.SysDict;
import com.prex.base.server.mapper.SysDictMapper;
import com.prex.base.server.service.ISysDictService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author lihaodong
 * @since 2019-05-17
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

    @Override
    public boolean save(SysDict entity) {
        return super.save(entity);
    }

    @Override
    public boolean updateDict(DictDTO dictDto) {
//        if (ObjectUtil.isNull(dictDto.getValue())) {
//            // 先查询所有的含有的主键 然后批量修改
//            List<SysDict> sysDicts = baseMapper.selectList(Wrappers.<SysDict>lambdaQuery().select(SysDict::getId).eq(SysDict::getName, baseMapper.selectById(dictDto.getId()).getName()));
//            List<SysDict> collect = sysDicts.stream().map(sysDict1 -> {
//                SysDict sysDict = new SysDict();
//                sysDict.setId(sysDict1.getId());
//                sysDict.setName(dictDto.getName());
//                return sysDict;
//            }).collect(Collectors.toList());
//            return updateBatchById(collect);
//        }
        SysDict sysDict = new SysDict();
        BeanUtil.copyProperties(dictDto, sysDict);
        return baseMapper.updateById(sysDict) > 0;
    }


    @Override
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }
}

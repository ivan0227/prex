package com.prex.auth.authentication.social.github.api;

/**
 * @Classname GitHub
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-07-08 22:04
 * @Version 1.0
 */
public interface GitHub {

    /**
     * 获取用户信息
     *
     * @return
     */
    GitHubUserInfo getUserInfo();
}
